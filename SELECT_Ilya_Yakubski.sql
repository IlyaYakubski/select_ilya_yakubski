---------------Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?------------

WITH RankedRevenue AS (
    SELECT
        s.store_id,
        s.staff_id,
        s.first_name || ' ' || s.last_name AS staff_name,
        SUM(p.amount) AS total_revenue,
        RANK() OVER (PARTITION BY s.store_id ORDER BY SUM(p.amount) DESC) AS revenue_rank
    FROM
        staff s
    JOIN payment p ON s.staff_id = p.staff_id
    WHERE
        EXTRACT(YEAR FROM p.payment_date) = 2017
    GROUP BY
        s.store_id, s.staff_id, staff_name
)
SELECT
    store_id,
    staff_id,
    staff_name,
    total_revenue
FROM
    RankedRevenue
WHERE
    revenue_rank = 1;

----------------------------------------------------------------------------------------

SELECT
    s.store_id,
    s.staff_id,
    s.first_name || ' ' || s.last_name AS staff_name,
    SUM(p.amount) AS total_revenue
FROM
    staff s
JOIN payment p ON s.staff_id = p.staff_id
WHERE
    EXTRACT(YEAR FROM p.payment_date) = 2017
GROUP BY
    s.store_id, s.staff_id, staff_name
HAVING
    SUM(p.amount) = (
        SELECT
            MAX(total_revenue)
        FROM
            (
                SELECT
                    store_id,
                    s.staff_id,
                    SUM(amount) AS total_revenue
                FROM
                    staff s
                JOIN payment p ON s.staff_id = p.staff_id
                WHERE
                    EXTRACT(YEAR FROM p.payment_date) = 2017
                GROUP BY
                    store_id, s.staff_id
            ) AS subquery
        WHERE
            subquery.store_id = s.store_id
    );







------------------Which five movies were rented more than the others, and what is the expected age of the audience for these movies?----------------

WITH MovieRentals AS (
    SELECT
        f.film_id,
        f.title,
        f.rating,
        COUNT(r.rental_id) AS rental_count
    FROM
        film f
    JOIN rental r ON f.film_id = r.inventory_id
    GROUP BY
        f.film_id, f.title, f.rating
)
SELECT
    m.title,
    m.rating,
    m.rental_count,
    CASE
        WHEN m.rating = 'PG' THEN '0+'
        WHEN m.rating = 'G' THEN '6+ without advisor'
        WHEN m.rating = 'PG-13' THEN '13+ without an advisor'
        WHEN m.rating = 'R' THEN '18+ without an advisor'
        WHEN m.rating = 'NC-17' THEN '18+ only'
        ELSE 'Unknown'
    END AS expected_age_group
FROM
    MovieRentals m
ORDER BY
    m.rental_count DESC
LIMIT 5;

--------------------------------------------------------------------------------------------------------

SELECT
    f.title,
    f.rating,
    (
        SELECT COUNT(r.rental_id)
        FROM rental r
        WHERE r.inventory_id = f.film_id
    ) AS rental_count,
    CASE
        WHEN f.rating = 'PG' THEN '0+'
        WHEN f.rating = 'G' THEN '6+ without advisor'
        WHEN f.rating = 'PG-13' THEN '13+ without an advisor'
        WHEN f.rating = 'R' THEN '18+ without an advisor'
        WHEN f.rating = 'NC-17' THEN '18+ only'
        ELSE 'Unknown'
    END AS expected_age_group
FROM
    film f
ORDER BY
    rental_count DESC
LIMIT 5;




------------------Which actors/actresses didn't act for a longer period of time than the others?-----------

SELECT
    actor_id,
    first_name,
    last_name,
    MAX(release_year) AS last_film_year,
    MAX(prev_release_year) AS prev_last_film_year,
    MAX(release_year) - MAX(prev_release_year) AS years_since_last_act
FROM (
    SELECT
        a.actor_id,
        a.first_name,
        a.last_name,
        f.release_year,
        LAG(f.release_year) OVER (PARTITION BY a.actor_id ORDER BY f.release_year) AS prev_release_year
    FROM
        actor a
    JOIN film_actor fa ON a.actor_id = fa.actor_id
    JOIN film f ON fa.film_id = f.film_id
) AS actor_film_years
GROUP BY
    actor_id, first_name, last_name
ORDER BY
    years_since_last_act DESC
LIMIT 5;

------------------------------------------------------------------------------------------------------

WITH actor_film_years AS (
    SELECT
        a.actor_id,
        a.first_name,
        a.last_name,
        f.release_year,
        LAG(f.release_year) OVER (PARTITION BY a.actor_id ORDER BY f.release_year) AS prev_release_year
    FROM
        actor a
    JOIN
        film_actor fa ON a.actor_id = fa.actor_id
    JOIN
        film f ON fa.film_id = f.film_id
)
SELECT
    actor_id,
    first_name,
    last_name,
    MAX(release_year) AS last_film_year,
    MAX(prev_release_year) AS prev_last_film_year,
    MAX(release_year) - MAX(prev_release_year) AS years_since_last_act
FROM
    actor_film_years
GROUP BY
    actor_id, first_name, last_name
ORDER BY
    years_since_last_act DESC
LIMIT 5;
